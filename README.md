# Humanity-EU <img src="folder-grey.svg" height="60" width="60"><img src="folder-home.svg" height="60" width="60"><img src="folder-trash.svg" height="60" width="60"> Icon Theme for gnome 3

gnome icon theme with symbols better recognizable in Europe.

## Installation as root:

create folder /usr/share/icons/Humanity-EU
copy svg files to /usr/share/icons/Humanity-EU
create additional symbolic links:<BR>
&emsp;gnome-mime-x-directory-smb-workgroup.svg -> network-workgroup.svg<BR>
&emsp;inode-directory-symbolic.svg -> /home/joachim/Dokumente/icons/Humanity-EU/folder-symbolic.svg<BR>
&emsp;user-desktop-symbolic.svg -> /home/joachim/Dokumente/icons/Humanity-EU/folder-symbolic.svg<BR>

create icon-caches by running (Debian, Ubuuntu…)
&emsp;<pre>update-icon-cache /usr/share/icons/Humanity-EU<BR></pre>
<BR>
In order to utilize it you need to quit all nautilus instances by running
&emsp;<pre>nautilus -q<BR></pre>

In order to activate the Theme you need to run the gnome-shell 3 extension »tweaks« (formally tweak tool).
It may be necessary to install it first from repositories.
As user you need to run it either from action menu ot from a terminal you would run:
&emsp;<pre>/usr/bin/python3 /usr/bin/gnome-tweaks</pre>

Under »appearance« you can choose the icon theme. Icons displayed in nautilus should follow immediately.
Should look like this:<BR> <img src="gnome-tweak-setting.png">

On systems other than Debian/Ubuntu, installation works differently. 
